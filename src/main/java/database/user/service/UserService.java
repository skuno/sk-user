package database.user.service;

import database.role.domain.UserRole;
import database.status.domain.UserStatus;
import database.token.domain.TokenRequest;
import database.token.domain.TokenResponse;
import database.user.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User saveOrUpdate(User user, UserRole userRole, UserStatus userStatus);

    void deleteById(Long id);

    Optional<User> findById(Long id);

    List<User> findAll();

    User banUser(Long userId, Long adminId);

    Optional<TokenResponse> login(TokenRequest tokenRequest);

}
