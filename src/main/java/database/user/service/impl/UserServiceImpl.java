package database.user.service.impl;

import database.role.domain.UserRole;
import database.role.service.UserRoleService;
import database.status.domain.UserStatus;
import database.status.service.UserStatusService;
import database.token.domain.TokenRequest;
import database.token.domain.TokenResponse;
import database.token.service.TokenService;
import database.user.domain.BanHistory;
import database.user.domain.User;
import database.user.repository.UserRepository;
import database.user.service.UserService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    private final UserRoleService userRoleService;

    private final UserStatusService userStatusService;

    private final TokenService tokenService;

    public UserServiceImpl(UserRepository repository,
                           UserRoleService userRoleService,
                           UserStatusService userStatusService,
                           TokenService tokenService) {
        this.repository = repository;
        this.userRoleService = userRoleService;
        this.userStatusService = userStatusService;
        this.tokenService = tokenService;
    }

    @Override
    public User saveOrUpdate(User user, UserRole userRole, UserStatus userStatus) {
        fillUserInfo(user);

        if (userRoleDoesntExist(userRole))
            user.setUserRole(userRoleService.saveOrUpdate(userRole));

        if (userStatusDoesntExist(userStatus))
            user.setUserStatus(userStatusService.saveOrUpdate(userStatus));

        return repository.save(user);
    }

    private void fillUserInfo(User user) {
        if (user.getFirstName() == null || user.getFirstName().isEmpty())
            user.setFirstName("Not provided");

        if (user.getLastName() == null || user.getLastName().isEmpty())
            user.setLastName("Not provided");

        if (user.getNumberOfReservations() == null)
            user.setNumberOfReservations(0);
    }

    private boolean userRoleDoesntExist(UserRole userRole) {
        return userRole == null || userRole.getId() == null ||
                !userRoleService.findById(userRole.getId()).isPresent();
    }

    private boolean userStatusDoesntExist(UserStatus userStatus) {
        return userStatus == null || userStatus.getId() == null ||
                !userStatusService.findById(userStatus.getId()).isPresent();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User banUser(Long userId, Long adminId) {
        User unluckyUser = repository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("User not found for id: " + userId));

        BanHistory banHistory = new BanHistory();
        banHistory.setAdminId(adminId);
        banHistory.setDate(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()));

        unluckyUser.setBanned(true);
        unluckyUser.getBanHistory().add(banHistory);

        return repository.save(unluckyUser);
    }

    @Override
    public Optional<TokenResponse> login(TokenRequest tokenRequest) {
        return repository.findAll().stream()
                .filter(user -> userFilter(user, tokenRequest.getUsername(), tokenRequest.getPassword()))
                .findFirst()
                .map(tokenService::generate);
    }

    private boolean userFilter(User user, String username, String password) {
        return user.getUsername().equals(username) &&
                BCrypt.checkpw(password, user.getPassword()) &&
                !user.isBanned();
    }
}
