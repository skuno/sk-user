package database.user.rest;

import database.token.domain.TokenRequest;
import database.token.domain.TokenResponse;
import database.token.security.CheckSecurity;
import database.user.domain.User;
import database.user.service.UserService;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/user")
public class UserController {

    private final UserService service;

    private final RestUtilities restUtilities;

    public UserController(UserService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @ApiOperation("Saves the given user or if it already exists it will be updated")
    public ResponseEntity<?> saveOrUpdate(@Valid @RequestBody User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        if (user.getUserRole() == null || user.getUserStatus() == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(service.saveOrUpdate(user, user.getUserRole(), user.getUserStatus()), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{userId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Delete user for the given user id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long userId) {

        System.err.println(userId);
        service.deleteById(userId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find user for the given id")
    public ResponseEntity<User> findById(@RequestHeader("Authorization") String authorization,
                                         @PathVariable Long userId) {
        return service.findById(userId)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all users")
    public ResponseEntity<List<User>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @PutMapping("/ban/{userId}/{adminId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Ban user with the given userId")
    public ResponseEntity<?> banUser(@RequestHeader("Authorization") String authorization,
                                     @PathVariable Long userId, @PathVariable Long adminId) {
        return new ResponseEntity<>(service.banUser(userId, adminId), HttpStatus.OK);
    }

    @PostMapping("/login")
    @ApiOperation(value = "User login and token generation")
    public ResponseEntity<TokenResponse> loginUser(@Valid @RequestBody TokenRequest tokenRequest) {
        return service.login(tokenRequest)
                .map(tokenResponse -> new ResponseEntity<>(tokenResponse, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
