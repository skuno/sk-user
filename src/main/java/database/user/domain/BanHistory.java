package database.user.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Embeddable
@Data
@NoArgsConstructor
public class BanHistory {

    private Long adminId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public String getDate() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date);
    }

    public void setDate(String date) {
        try {
            this.date = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
