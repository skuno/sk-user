package database.user.domain;

import database.role.domain.UserRole;
import database.status.domain.UserStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_role")
    private UserRole userRole;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_status")
    private UserStatus userStatus;

    @NotNull
    @NotBlank
    @Column(name = "username", unique = true)
    private String username;

    @NotNull
    private String password;

    @Email
    @NotNull
    private String email;

    @PositiveOrZero
    private Integer numberOfReservations;

    private String firstName;

    private String lastName;

    private boolean banned;

    @ElementCollection
    private List<BanHistory> banHistory;

    private static final int MIN_PASSWORD_LENGTH = 8;

    private static final int MAX_PASSWORD_LENGTH = 30;

    private boolean isPasswordValid(String plainText) {
        return plainText != null
                && plainText.length() >= MIN_PASSWORD_LENGTH
                && plainText.length() <= MAX_PASSWORD_LENGTH;
    }

    public void setPassword(String plainText) {
        // hashed value was sent
        if (plainText.length() > MAX_PASSWORD_LENGTH) {
            this.password = plainText;
            return;
        }

        if (!isPasswordValid(plainText))
            throw new IllegalArgumentException("Password is invalid");

        this.password = BCrypt.hashpw(plainText, BCrypt.gensalt());
    }

}
