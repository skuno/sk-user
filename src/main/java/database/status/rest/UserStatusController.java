package database.status.rest;

import database.status.domain.UserStatus;
import database.status.service.UserStatusService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/userStatus")
public class UserStatusController {

    private final UserStatusService service;

    private final RestUtilities restUtilities;

    public UserStatusController(UserStatusService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Save the given user status or update it if it already exists")
    public ResponseEntity<?> save(@RequestHeader("Authorization") String authorization,
                                  @Valid @RequestBody UserStatus userStatus, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        return new ResponseEntity<>(service.saveOrUpdate(userStatus), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{userStatusId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Delete a user status for the given user status id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long userStatusId) {
        service.deleteById(userStatusId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{userStatusId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Find user status for the given id")
    public ResponseEntity<UserStatus> findById(@RequestHeader("Authorization") String authorization,
                                               @PathVariable Long userStatusId) {
        return service.findById(userStatusId)
                .map(userStatus -> new ResponseEntity<>(userStatus, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/name/{userStatusName}")
    @ApiOperation("Find user status for the given name")
    public ResponseEntity<UserStatus> findByName(@PathVariable String userStatusName) {
        return service.findByName(userStatusName)
                .map(userStatus -> new ResponseEntity<>(userStatus, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Find all user statuses")
    public ResponseEntity<List<UserStatus>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }
}
