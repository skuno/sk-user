package database.status.service;

import database.status.domain.UserStatus;

import java.util.List;
import java.util.Optional;

public interface UserStatusService {

    UserStatus saveOrUpdate(UserStatus userStatus);

    void deleteById(Long id);

    Optional<UserStatus> findById(Long id);

    Optional<UserStatus> findByName(String name);

    List<UserStatus> findAll();

}
