package database.status.service.impl;

import database.status.domain.UserStatus;
import database.status.repository.UserStatusRepository;
import database.status.service.UserStatusService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserStatusServiceImpl implements UserStatusService {

    private final UserStatusRepository repository;

    public UserStatusServiceImpl(UserStatusRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserStatus saveOrUpdate(UserStatus userStatus) {
        return repository.save(userStatus);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<UserStatus> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<UserStatus> findByName(String name) {
        return repository.findAll().stream()
                .filter(userRole -> userRole.getName().name().equalsIgnoreCase(name))
                .findFirst();
    }

    @Override
    public List<UserStatus> findAll() {
        return repository.findAll();
    }
}
