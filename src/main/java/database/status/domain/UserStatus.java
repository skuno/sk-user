package database.status.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

@Entity
@Table(name = "user_status")
@Data
@NoArgsConstructor
public class UserStatus implements Serializable {

    @Id
    @Column(name = "user_status_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusName name;

    @NotNull
    @PositiveOrZero
    private Integer requiredPoints;

    @NotNull
    @PositiveOrZero
    private Integer maximumPoints;

    @NotNull
    @PositiveOrZero
    private Float discount;

}
