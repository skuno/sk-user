package database.status.domain;

public enum StatusName {

    REGULAR, GOLD, DIAMOND

}
