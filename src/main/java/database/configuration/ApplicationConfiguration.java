package database.configuration;

import database.util.RestUtilities;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public RestUtilities restUtilities() {
        return new RestUtilities();
    }

}
