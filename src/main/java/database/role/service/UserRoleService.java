package database.role.service;

import database.role.domain.UserRole;

import java.util.List;
import java.util.Optional;

public interface UserRoleService {

    UserRole saveOrUpdate(UserRole userRole);

    void deleteById(Long id);

    Optional<UserRole> findById(Long id);

    Optional<UserRole> findByName(String name);

    List<UserRole> findAll();

}
