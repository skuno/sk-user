package database.role.service.impl;

import database.role.domain.UserRole;
import database.role.repository.UserRoleRepository;
import database.role.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository repository;

    public UserRoleServiceImpl(UserRoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserRole saveOrUpdate(UserRole userRole) {
        if (userRole.getDescription() == null || userRole.getDescription().isEmpty())
            userRole.setDescription("Not provided");

        return repository.save(userRole);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<UserRole> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<UserRole> findByName(String name) {
        return repository.findAll().stream()
                .filter(userRole -> userRole.getName().name().equalsIgnoreCase(name))
                .findFirst();
    }

    @Override
    public List<UserRole> findAll() {
        return repository.findAll();
    }
}
