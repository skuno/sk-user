package database.role.domain;

public enum RoleName {

    USER, ADMIN

}
