package database.role.rest;

import database.role.domain.UserRole;
import database.role.service.UserRoleService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/userRole")
public class UserRoleController {

    private final UserRoleService service;

    private final RestUtilities restUtilities;

    public UserRoleController(UserRoleService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Save the given user role, if it already exists it will be updated")
    public ResponseEntity<?> saveOrUpdate(@RequestHeader("Authorization") String authorization,
                                          @Valid @RequestBody UserRole userRole, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        return new ResponseEntity<>(service.saveOrUpdate(userRole), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{userRoleId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Delete a user role for the given id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long userRoleId) {
        service.deleteById(userRoleId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{userRoleId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Find a user role for the given user role id")
    public ResponseEntity<UserRole> findById(@RequestHeader("Authorization") String authorization,
                                             @PathVariable Long userRoleId) {
        return service.findById(userRoleId)
                .map(userRole -> new ResponseEntity<>(userRole, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/name/{userRoleName}")
    @ApiOperation("Find a user role for the given user role name")
    public ResponseEntity<UserRole> findByName(@PathVariable String userRoleName) {
        return service.findByName(userRoleName)
                .map(userRole -> new ResponseEntity<>(userRole, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Find all user roles")
    public ResponseEntity<List<UserRole>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

}
